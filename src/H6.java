import java.util.Scanner;

public class H6 {

	public static void main(String[] args) {
		// 输入10个数，求他们当中最大的数
		int i = 1;
		double b = 0;
		double max = 0;
		Scanner A = new Scanner(System.in);
		while (i <= 10) {
			System.out.print("输入数字");
			double a = A.nextDouble();
			max = a > b ? a : b;
			b = max;
			i++;
		}
		System.out.print(max);
	}

}
