public class H2 {

	public static void main(String[] args) {
		// 输出1，2，3，4，。。。一直到100的立方
		int i = 1;
		while (i <= 100) {
			int a = (int) (Math.pow(i, 3));
			System.out.print(a + " ");
			i++;
		}
	}

}
