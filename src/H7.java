import java.util.Scanner;

public class H7 {

	public static void main(String[] args) {
		// 找到所有的四维水仙花数:
		// 水仙花数是这样的一个四位数：
		// 这个数等于每一位上的数的四次方和
		Scanner A = new Scanner(System.in);
		System.out.print("输入水仙花的四维");
		double a = A.nextDouble(), s = 0;
		while (!(a == 0)) {
			s = s + Math.pow(a % 10, 4);
			a = (int) a / 10;
		}
		System.out.print(s);
	}

}
